package printapplet;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import org.apache.commons.io.FileUtils;


public class PrintApplet {
    
    public static void main(String[] args) throws IOException, PrinterException {
        // TODO code application logic here           
                        
    }
    
    public void printFunction(String[] links) throws MalformedURLException, IOException, PrinterException {
        
        ArrayList<URL> url = new ArrayList<URL>();
        
        for (int i = 0; i < links.length; i++) {
            url.add(new URL(links[i]));
        }
        
        File f = new File("C:/Users/Personal/Documents/file.pdf");
        for (int i = 0; i < url.size(); i++) {
            FileUtils.copyURLToFile(url.get(i), f);
            PDDocument doc;
            doc = PDDocument.load(f);
            PrinterJob job = PrinterJob.getPrinterJob();
            job.setPageable(new PDFPageable(doc));
            job.print();
      }  
    }
    
}
